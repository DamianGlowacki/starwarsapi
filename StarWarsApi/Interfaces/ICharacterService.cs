﻿using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Interfaces
{
    public interface ICharacterService
    {
        Task<bool> CreateCharacter(Character character);
        Task<Character> UpdateCharacter(int characterId, Character character);
        Task<Character> GetCharacterByName(string characterName);
        Task<Character> DeleteCharacterByName(string characterName);
        Task<IEnumerable<Character>> GetAllCharacters();
        Task<Character> AttachEpisodeToCharcter(string episodeName, string characterName);
        Task<Character> AttachFriendToCharcter(string friendName, string characterName);
    }
}
