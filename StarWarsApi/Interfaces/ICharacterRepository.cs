﻿using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Interfaces
{
    public interface ICharacterRepository
    {
        Task<Character> CreateCharacter(CharacterEntity characterEntity);
        Task<Character> UpdateCharacter(int characterId, CharacterEntity characterEntity);
        Task<Character> GetCharacterByName(string characterName);
        Task<Character> DeleteCharacterByName(string characterName);
        Task<IEnumerable<Character>> GetAllCharacters();
        Task<Character> AttachEpisodeToCharcter(string episodeName, string characterName);
        Task<Character> AttachFriendToCharcter(string friendName, string characterName);
    }
}
