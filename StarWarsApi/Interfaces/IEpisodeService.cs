﻿using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Interfaces
{
    public interface IEpisodeService
    {
        Task<bool> CreateEpisode(Episode episode);
        Task<Episode> DeleteEpisode(Episode episode);
    }
}
