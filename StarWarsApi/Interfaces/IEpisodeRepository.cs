﻿using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Interfaces
{
    public interface IEpisodeRepository
    {
        Task<EpisodeEntity> CreateEpisode(EpisodeEntity episode);
        Task<EpisodeEntity> DeleteEpisode(EpisodeEntity episode);
    }
}
