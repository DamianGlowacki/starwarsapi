﻿using StarWarsApi.Interfaces;
using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Services
{
    public class EpisodeService : IEpisodeService
    {
        #region Private fields

        private readonly IEpisodeRepository _episodeRepository;
        private readonly MappingService _mappingService;

        #endregion

        #region Ctor

        public EpisodeService(IEpisodeRepository episodeRepository)
        {
            _episodeRepository = episodeRepository;
            MappingService mappingService = new MappingService();
            _mappingService = mappingService;
        }

        #endregion

        #region Public methods

        public async Task<bool> CreateEpisode(Episode episode)
        {
            if (episode == null)
            {
                return false;
            }
            else
            {
                var result = await _episodeRepository.CreateEpisode(_mappingService.MapEpisodeToEpisodeEntity(episode));
                if (result != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public async Task<Episode> DeleteEpisode(Episode episode)
        {
            if (episode == null)
            {
                return null;
            }
            else
            {
                var result = await _episodeRepository.DeleteEpisode(_mappingService.MapEpisodeToEpisodeEntity(episode));
                return _mappingService.MapEpisodeEntityToEpisode(result);
            }
        }

        #endregion
    }
}
