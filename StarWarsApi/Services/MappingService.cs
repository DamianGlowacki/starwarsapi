﻿using AutoMapper;
using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Services
{
    public class MappingService
    {
        public CharacterEntity MapCharacterToCharacterEntity(Character character)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Character, CharacterEntity>();
            });
            IMapper iMapper = config.CreateMapper();

            return iMapper.Map<Character, CharacterEntity>(character);
        }

        public Character MapCharacterEntityToCharacter(CharacterEntity character)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CharacterEntity, Character>();
            });
            IMapper iMapper = config.CreateMapper();

            return iMapper.Map<CharacterEntity, Character>(character);
        }

        public Episode MapEpisodeEntityToEpisode(EpisodeEntity entity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<EpisodeEntity, Episode>();
            });
            IMapper iMapper = config.CreateMapper();

            return iMapper.Map<EpisodeEntity, Episode>(entity);
        }

        public EpisodeEntity MapEpisodeToEpisodeEntity(Episode entity)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Episode, EpisodeEntity>();
            });
            IMapper iMapper = config.CreateMapper();

            return iMapper.Map<Episode, EpisodeEntity>(entity);
        }

        public List<Character> MapCharacterEntityToCharacterList(IEnumerable<CharacterEntity> entities)
        {
            List<Character> characters = new List<Character>();

            foreach (var entity in entities)
            {
                characters.Add(MapCharacterEntityToCharacter(entity));
            }

            return characters;
        }
    }
}
