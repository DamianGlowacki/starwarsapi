﻿using StarWarsApi.Interfaces;
using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Services
{
    public class CharacterService : ICharacterService
    {
        #region Private fields

        private readonly ICharacterRepository _characterRepository;
        private readonly MappingService _mappingService;

        #endregion

        #region Ctor

        public CharacterService(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
            MappingService mappingService = new MappingService();
            _mappingService = mappingService;
        }

        #endregion

        #region Public methods

        public async Task<bool> CreateCharacter(Character character)
        {
            try
            {
                if (character == null)
                {
                    return false;
                }
                else
                {
                    var result = await _characterRepository.CreateCharacter(_mappingService.MapCharacterToCharacterEntity(character));
                    if (result != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //Logger.Debug.Format($"[CharacterService] CreateCharacter exception {ex}");
                return false;
            }
        }

        public async Task<Character> UpdateCharacter(int characterId, Character character)
        {
            try
            {
                if (characterId <= 0 || character == null)
                {
                    return null;
                }
                else
                {
                    return await _characterRepository.UpdateCharacter(characterId, _mappingService.MapCharacterToCharacterEntity(character));
                }
            }
            catch (Exception ex)
            {
                //Logger.Error.Format($"[CharacterService] UpdateCharacter exception {ex}");
                return null;
            }
        }

        public async Task<Character> GetCharacterByName(string characterName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(characterName))
                {
                    return null;
                }
                else
                {
                    return await _characterRepository.GetCharacterByName(characterName);
                }
            }
            catch (Exception ex)
            {
                //Logger.Error.Format($"[CharacterService] GetCharacterByName exception {ex}");
                return null;
            }
        }

        public async Task<Character> DeleteCharacterByName(string characterName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(characterName))
                {
                    return null;
                }
                else
                {
                    return await _characterRepository.DeleteCharacterByName(characterName);                    
                }
            }
            catch (Exception ex)
            {
                //Logger.Error.Format($"[CharacterService] DeleteCharacterByName exception {ex}");
                return null;
            }

        }

        public async Task<IEnumerable<Character>> GetAllCharacters()
        {
            try
            {
                return await _characterRepository.GetAllCharacters();
            }
            catch (Exception ex)
            {
                //Logger.Error.Format($"[CharacterService] GetAllCharacters exception {ex}");
                return null;
            }
        }


        public async Task<Character> AttachEpisodeToCharcter(string episodeName, string characterName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(episodeName) || string.IsNullOrWhiteSpace(characterName))
                {
                    return null;
                }
                else
                {
                    return await _characterRepository.AttachEpisodeToCharcter(episodeName, characterName);
                }
            }
            catch (Exception ex)
            {
                //Logger.Error.Format($"[CharacterService] AttachEpisodeToCharcter exception {ex}");
                return null;
            }

        }


        public async Task<Character> AttachFriendToCharcter(string friendName, string characterName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(friendName) || string.IsNullOrWhiteSpace(characterName))
                {
                    return null;
                }
                else
                {
                    return await _characterRepository.AttachFriendToCharcter(friendName, characterName);
                }
            }
            catch (Exception ex)
            {
                //Logger.Error.Format($"[CharacterService] AttachEpisodeToCharcter exception {ex}");
                return null;
            }
        }

        #endregion
    }
}
