﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Models
{
    public class CharacterEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<EpisodeEntity> Episodes { get; set; }
        public string Planet { get; set; }
        public IEnumerable<CharacterEntity> Friends { get; set; }
    }
}
