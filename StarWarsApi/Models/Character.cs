﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Models
{
    public class Character
    {
        public string Name { get; set; }
        public List<string> Episodes { get; set; }
        public string Planet { get; set; }
        public List<string> Friends { get; set; }
    }
}
