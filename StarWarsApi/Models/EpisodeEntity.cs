﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Models
{
    public class EpisodeEntity
    {
        public int Id { get; set; }
        public string EpisodeName { get; set; }
    }
}
