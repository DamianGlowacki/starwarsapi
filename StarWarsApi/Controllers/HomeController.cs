﻿using Microsoft.AspNetCore.Mvc;

namespace StarWarsApi.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

    }
}