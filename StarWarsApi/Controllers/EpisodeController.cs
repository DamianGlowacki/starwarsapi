﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StarWarsApi.Interfaces;
using StarWarsApi.Models;

namespace StarWarsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EpisodeController : ControllerBase
    {
        private readonly IEpisodeService _episodeService;

        public EpisodeController(IEpisodeService episodeService)
        {
            _episodeService = episodeService;
        }

        [HttpPost("CreateEpisode")]
        public async Task<IActionResult> CreateEpisode([FromQuery] Episode episode)
        {
            if (await _episodeService.CreateEpisode(episode))
            {
                return Ok(HttpStatusCode.Created);
            }
            else
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost("DeleteEpisode")]
        public IActionResult DeleteEpisode([FromQuery] Episode episode)
        {
            var result = _episodeService.DeleteEpisode(episode);

            if (result == null)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            else
            {
                return Ok(result);
            }
        }
    }
}