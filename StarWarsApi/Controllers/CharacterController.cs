﻿using Microsoft.AspNetCore.Mvc;
using StarWarsApi.Interfaces;
using StarWarsApi.Models;
using System.Net;
using System.Threading.Tasks;

namespace StarWarsApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharacterController : Controller
    {
        private readonly ICharacterService _characterService;

        public CharacterController(ICharacterService characterService)
        {
            _characterService = characterService;
        }

        [HttpPost("CreateCharacter")]
        public async Task<IActionResult> CreateCharacter([FromQuery] Character character)
        {
            if (await _characterService.CreateCharacter(character))
            {
                return Ok(HttpStatusCode.Created);
            }
            else
            {
                return Ok(HttpStatusCode.BadRequest);
            }
        }

        [HttpPut("UpdateCharacter")]
        public async Task<IActionResult> UpdateCharacter([FromQuery] int characterId, [FromQuery] Character character)
        {
            var result = await _characterService.UpdateCharacter(characterId, character);

            if (result == null)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("GetCharacterByName")]
        public async Task<IActionResult> GetCharacterByName(string characterName)
        {
            var result = await _characterService.GetCharacterByName(characterName);

            if (result == null)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpDelete("DeleteCharacter")]
        public async Task<IActionResult> DeleteCharacterByName([FromQuery] string characterName)
        {
            var result = await _characterService.DeleteCharacterByName(characterName);

            if (result == null)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpGet("GetAllCharacters")]
        public async Task<IActionResult> GetAllCharacters()
        {
            var result = await _characterService.GetAllCharacters();

            return Ok(result);
        }

        [HttpPut("AttachEpisodeToCharcter")]
        public async Task<IActionResult> AttachEpisodeToCharcter(string episodeName, string characterName)
        {
            var result = await _characterService.AttachEpisodeToCharcter(episodeName, characterName);

            if (result == null)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            else
            {
                return Ok(result);
            }
        }

        [HttpPut("AttachFriendToCharcter")]
        public async Task<IActionResult> AttachFriendToCharcter(string friendName, string characterName)
        {
            var result = await _characterService.AttachFriendToCharcter(friendName, characterName);

            if (result == null)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            else
            {             
                return Ok(result);
            }
        }
    }
}