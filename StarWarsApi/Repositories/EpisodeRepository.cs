﻿using StarWarsApi.Data;
using StarWarsApi.Interfaces;
using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Repositories
{
    public class EpisodeRepository : IEpisodeRepository
    {
        #region Private fields

        private readonly ApplicationDbContext _applicationDbContext;

        #endregion

        #region Ctor

        public EpisodeRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        #endregion

        #region Public methods

        public async Task<EpisodeEntity> CreateEpisode (EpisodeEntity episode)
        {
            await _applicationDbContext.Episodes.AddAsync(episode);
            await _applicationDbContext.SaveChangesAsync();
            return episode;
        }

        public async Task<EpisodeEntity> DeleteEpisode(EpisodeEntity episode)
        {
            _applicationDbContext.Episodes.Remove(episode);
            await _applicationDbContext.SaveChangesAsync();
            return episode;
        }

        #endregion
    }
}
