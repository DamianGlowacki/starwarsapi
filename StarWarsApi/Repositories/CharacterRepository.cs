﻿using StarWarsApi.Data;
using StarWarsApi.Interfaces;
using StarWarsApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StarWarsApi.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        #region Private fields

        private readonly ApplicationDbContext _applicationDbContext;

        #endregion

        #region Ctor

        public CharacterRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        #endregion

        #region Public methods

        public async Task<Character> CreateCharacter(CharacterEntity characterEntity)
        {
            await _applicationDbContext.Characters.AddAsync(characterEntity);
            await _applicationDbContext.SaveChangesAsync();

            var result = new Character();
            result.Name = characterEntity.Name;
            result.Planet = characterEntity.Planet;
            result.Episodes = GetEpisodes(characterEntity);
            result.Friends = GetFriends(characterEntity);

            return result;
        }

        public async Task<Character> UpdateCharacter(int characterId, CharacterEntity characterEntity)
        {
            var characterToUpdate = _applicationDbContext.Characters.FirstOrDefault(x => x.Id == characterId);
            characterToUpdate = characterEntity;
            _applicationDbContext.Characters.Update(characterToUpdate);
            await _applicationDbContext.SaveChangesAsync();

            var characterUpdated = _applicationDbContext.Characters.FirstOrDefault(x => x.Id == characterId);

            var result = new Character();
            result.Name = characterUpdated.Name;
            result.Planet = characterUpdated.Planet;
            result.Episodes = GetEpisodes(characterUpdated);
            result.Friends = GetFriends(characterUpdated);

            return result;
        }

        public async Task<Character> GetCharacterByName(string characterName)
        {
            var character = _applicationDbContext.Characters.FirstOrDefault(x => x.Name == characterName);

            var result = new Character();
            result.Name = character.Name;
            result.Planet = character.Planet;
            result.Episodes = GetEpisodes(character);
            result.Friends = GetFriends(character);

            return result;
        }

        public async Task<Character> DeleteCharacterByName(string characterName)
        {
            var characterToRemove = _applicationDbContext.Characters.FirstOrDefault(x => x.Name == characterName);

            var result = new Character();
            result.Name = characterToRemove.Name;
            result.Planet = characterToRemove.Planet;
            result.Episodes = GetEpisodes(characterToRemove);
            result.Friends = GetFriends(characterToRemove);

            _applicationDbContext.Characters.Remove(characterToRemove);
            await _applicationDbContext.SaveChangesAsync();

            return result;
        }

        public async Task<IEnumerable<Character>> GetAllCharacters()
        {
            var characters = _applicationDbContext.Characters.ToList();

            List<Character> result = new List<Character>();

            foreach (var item in characters)
            {
                var character = new Character();
                character.Name = item.Name;
                character.Planet = item.Planet;
                character.Episodes = GetEpisodes(item);
                character.Friends = GetFriends(item);

                result.Add(character);
            }
            return result;
        }


        public async Task<Character> AttachEpisodeToCharcter(string episodeName, string characterName)
        {
            var character = _applicationDbContext.Characters.FirstOrDefault(x => x.Name == characterName);
            var episode = _applicationDbContext.Episodes.FirstOrDefault(x => x.EpisodeName == episodeName);

            List<EpisodeEntity> episodes = new List<EpisodeEntity>();

            if (character.Episodes != null)
            {
                episodes = character.Episodes.ToList();
            }

            episodes.Add(episode);

            character.Episodes = episodes;

            _applicationDbContext.Characters.Update(character);
            await _applicationDbContext.SaveChangesAsync();

            var result = new Character();
            result.Name = character.Name;
            result.Planet = character.Planet;
            result.Episodes = GetEpisodes(character);
            result.Friends = GetFriends(character);

            return result;
        }

        public async Task<Character> AttachFriendToCharcter(string friendName, string characterName)
        {
            var character = _applicationDbContext.Characters.FirstOrDefault(x => x.Name == characterName);
            var friend = _applicationDbContext.Characters.FirstOrDefault(x => x.Name == friendName);

            List<CharacterEntity> friends = new List<CharacterEntity>();

            if (character.Friends != null)
            {
                friends = character.Friends.ToList();
            }

            friends.Add(friend);

            character.Friends = friends;

            _applicationDbContext.Characters.Update(character);
            await _applicationDbContext.SaveChangesAsync();

            var result = new Character();
            result.Name = character.Name;
            result.Planet = character.Planet;
            result.Episodes = GetEpisodes(character);
            result.Friends = GetFriends(character);

            return result;
        }

        #endregion

        #region Private methods

        private List<string> GetEpisodes(CharacterEntity character)
        {
            if (character.Episodes == null)
            {
                return null;
            }

            List<string> episodes = new List<string>();

            foreach (var item in character.Episodes)
            {
                episodes.Add(item.EpisodeName);
            }

            return episodes;
        }

        private List<string> GetFriends(CharacterEntity character)
        {
            if (character.Friends == null)
            {
                return null;
            }

            List<string> friends = new List<string>();

            foreach (var item in character.Friends)
            {
                friends.Add(item.Name);
            }

            return friends;
        }

        #endregion
    }
}
